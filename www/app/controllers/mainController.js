app.controller ("mainController", function($scope, $rootScope, $localStorage, $state){
  if ($localStorage.login)
    $scope.login = $localStorage.login;

  $scope.setLoginData = function(login){
    $scope.login = login;
    $localStorage.login = login;
    if (!login)
      $state.go ('stateIndex');
  };
});
