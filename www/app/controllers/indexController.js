app.controller ('indexController', function ($scope, $rootScope, $http, $state){
	  $scope.spettacoloSelezionato = null;
	  $scope.attoreSelezionato = null;
	  $scope.attoriCaricati = false;

	  if ($scope.login){
			$state.go ('stateRipasso');
		}
	  else{
	  	$http.get("http://giuseppevavala.pythonanywhere.com/rest/spettacoli")
		  	.success(function(response) {
					$scope.spettacoli = response;
				});

			$scope.setSpettacoloSelezionato = function (val){
				if (val){
					$scope.spettacoloSelezionato = val;
					$http.get("http://giuseppevavala.pythonanywhere.com/rest/spettacolo/" + val + "/attori")
				  	.success(function(response) {
							$scope.attori = response;
							$scope.attoriCaricati = true;
					});
				}
			};

			$scope.setAttoreSelezionato = function (val){
				$scope.attoreSelezionato = val;
			};

			$scope.saveStateAndGoToRipasso = function(){
				$scope.setLoginData({'attore': $scope.attoreSelezionato, 'spettacolo': $scope.spettacoloSelezionato});
				$state.go ('stateRipasso');
			}
	  }
  });
