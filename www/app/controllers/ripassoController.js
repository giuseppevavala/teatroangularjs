app.controller ('ripassoController', function ($scope, $rootScope, $http, $state){
  $scope.spettacolo = $scope.login['spettacolo'];
	$scope.attore = $scope.login['attore'];
  $scope.indexBattuta = 0;
  $scope.miaBattuta = null;
  $scope.BATTUTE_DA_VISUALIZZARE = 3;

  $http.get("http://giuseppevavala.pythonanywhere.com/rest/spettacolo/" + $scope.spettacolo)
    .success(function(response) {
      $scope.copione = response['copione'];
      $scope.prossimaBattuta();
    });

  $scope.prossimaBattuta = function(){
    $scope.miaBattutaShowed = false;
    $scope.battute = [];

    var ind = -1;
    for (var i = $scope.indexBattuta + 1; (i < $scope.copione.length) && (ind == -1); i++)
      if ($scope.copione[i].name == $scope.attore)
        ind = i;

    if (ind >= 0){
      $scope.indexBattuta = ind;
      $scope.miaBattuta = $scope.copione[ind];
      for (var i=$scope.BATTUTE_DA_VISUALIZZARE; i > 0; i--)
        if ((i >= 0) && ($scope.copione[ind - i]))
          $scope.battute.push ($scope.copione[ind - i]);
    }else {
      $scope.indexBattuta = 0;
      $scope.prossimaBattuta();
    }
  }
});
