'use strict';

var app = angular.module('teatrantiApp', ['ngStorage', 'ui.router', 'ngTouch'])
	.filter('capitalize', function() {
		return function(input, all) {
			return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
		}
	})
	.config(function($stateProvider, $urlRouterProvider) {
	  $urlRouterProvider.otherwise("/state1");
	  $stateProvider
	    .state('stateIndex', {
	      url: "/state1",
	      templateUrl: "app/partials/stateIndex.html",
	      controller: "indexController"
	    })
	    .state('stateRipasso', {
	      url: "/state2",
	      templateUrl: "app/partials/stateRipasso.html",
	      controller: "ripassoController"
	    });
	});
